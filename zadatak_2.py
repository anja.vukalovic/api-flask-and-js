from tkinter import E
from flask import Flask
from flask_restful import Resource, Api, reqparse
import pymysql.cursors

#treba da se povezemo sa nasom sql bazom

connection = pymysql.connect(host = 'localhost',
                            user = 'root',
                            password = '8520',
                            database = 'classicmodels',
                            charset = 'utf8mb4',
                            cursorclass = pymysql.cursors.DictCursor)
    
cursor=connection.cursor()


app = Flask(__name__)
api= Api(app)

#trebaju nam dvije rute /employees i /office/office_code
class Employees(Resource):
    def get(self):
        #get request treba da nam vrati imena zaposlenih,email,uloga,imena nadleznog i office kod
        cursor.execute("select e1.firstName,e1.lastName,e1.email,e1.jobTitle,e1.officeCode, concat(e2.firstName,' ', e2.lastName) as 'reports to' from employees e1, employees e2 where e1.reportsTo= e2.employeeNumber")

        rez=cursor.fetchall()
        #print(type(rez))
        return rez
    def post(self):
        #post request treba da doda novog radnika u tabelu employee
        parser=reqparse.RequestParser()
        parser.add_argument('employeeNumber',required=True, type=int)
        parser.add_argument('lastName',required=True, type=str)
        parser.add_argument('firstName',required=True, type=str)
        parser.add_argument('extension',required=True, type=str)
        parser.add_argument('email',required=True, type=str)
        parser.add_argument('officeCode',required=True, type=str)
        parser.add_argument('reportsTo',required=True, type=int)
        parser.add_argument('jobTitle',required=True, type=str)
        args = parser.parse_args()
        cursor.execute(f"insert into employees(employeeNumber,lastName,firstName,extension,email, \
                        officeCode,reportsTo,jobTitle) values ({args['employeeNumber']}, '{args['lastName']}', \
                            '{args['firstName']}', '{args['extension']}', '{args['email']}', '{args['officeCode']}', \
                                {args['reportsTo']}, '{args['jobTitle']}')")
        connection.commit()

    def delete(self):
        #delete request treba da brise radnika iz tabele employee
        parser=reqparse.RequestParser()
        parser.add_argument('employeeNumber',required=True, type=int)
        args = parser.parse_args()
        cursor.execute(f"delete from employees where employeeNumber = {args['employeeNumber']}")
        connection.commit()
    
    def put(self):
        #potrebno je napraviti update neke kolone za radnika ciji znamo employeeNumber 
        parser=reqparse.RequestParser()
        parser.add_argument('employeeNumber',required=True, type=int)
        args = parser.parse_args()
        



        

        



api.add_resource(Employees,'/employees')



if __name__ == '__main__':
    app.run(debug=True)

